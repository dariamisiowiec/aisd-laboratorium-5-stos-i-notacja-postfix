package lab5;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileHandler {

    private Scanner scanner;
    private PrintWriter writer;


    public String fromFileToString (String fileName) {
        String readLine = "";
                try {
            scanner = new Scanner(new File(fileName)); {
                readLine = scanner.nextLine();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return readLine;
    }

    public void fromStringListToFile (List<String> inputList, String filename) {
        try {
            writer = new PrintWriter(new FileWriter(filename, true));

            for (int i = 0; i < inputList.size(); i++) {
                writer.print(inputList.get(i));
            }
            writer.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


}
