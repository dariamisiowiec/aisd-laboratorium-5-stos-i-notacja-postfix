package lab5;

import java.util.LinkedList;
import java.util.List;

public class PostfixConverter {
    private InputAnalyser inputAnalyser;

    public PostfixConverter(){
        inputAnalyser = new InputAnalyser();
    }

    public List<String> convertToPostfix (List<String> inputList){

        Stack<String> stack = new Stack<>();
        List<String> postfixExpression = new LinkedList<>();

        for (int i = 0; i < inputList.size(); i++) {
            if (inputAnalyser.isOperand(inputList.get(i))) {
                postfixExpression.add(inputList.get(i));
            }
            else if (inputList.get(i).equals("(")) {
                stack.push(inputList.get(i));
            }
            else if (inputList.get(i).equals(")")) {
                while (!stack.isEmpty() && !stack.peek().equals("(")) {
                    postfixExpression.add(stack.pop());
                }
                if (!stack.isEmpty() && !stack.peek().equals("(")){
                    System.out.println("Invalid input expression");
                }
                else
                    stack.pop();
            }
            else {
                while (!stack.isEmpty() &&
                        inputAnalyser.getPrecedence(inputList.get(i))<inputAnalyser.getPrecedence(stack.peek())) {
                    postfixExpression.add(stack.pop());
                }
                stack.push(inputList.get(i));
            }
        }
        while (!stack.isEmpty()) {
            postfixExpression.add(stack.pop());
        }

        return postfixExpression;
    }
}
