package lab5;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class InputAnalyser {

    public boolean isOperand(String input){
        switch (input) {
            case "+":
            case "-":
            case "*":
            case "/":
            case "(":
            case ")":
                return false;
        }
        return true;
    }


    public int getPrecedence(String input){

            switch (input) {
                case "+":
                case "-":
                    return 1;
                case "*":
                case "/":
                    return 2;
            }

        return 0;
    }

    public List<String> convertFromStringToList (String input) {
        String[] table = input.split("");
        List<String> bufer = new LinkedList<>();
        List<String> convertedList = new LinkedList<>();
        for (int i = 0; i < table.length; i++) {
            if (!isOperand(table[i])) {
                if (bufer.size() != 0) {
                    String string = "";
                    for (int j = 0; j < bufer.size(); j++) {
                        string += bufer.get(j);
                    }
                    convertedList.add(string);
                    bufer.clear();
                }
                convertedList.add(table[i]);
            }
            else
                bufer.add(table[i]);
        }
        if (!bufer.isEmpty()) {
            String string = "";
            for (int j = 0; j < bufer.size(); j++) {
                string += bufer.get(j);
            }
            convertedList.add(string);
        }
        return convertedList;
    }

    public List<String> convertFromStringToListForPostfix (String input) {

        String[] temp = input.split(",");
        List<String> output = new LinkedList<>();

        for (int i = 0; i < temp.length; i++) {
            output.add(temp[i]);
        }

        return output;
    }
}
