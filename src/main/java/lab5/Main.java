package lab5;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
     FileHandler fileHandler = new FileHandler();
     PostfixConverter postfixConverter = new PostfixConverter();
     InputAnalyser inputAnalyser = new InputAnalyser();

     String inputString =  fileHandler.fromFileToString("C:\\Users\\dasza\\Projects\\pg\\AiSD\\AiSDLab5\\src\\main\\java\\lab5\\infixInput.txt");
     List<String> inputList = inputAnalyser.convertFromStringToList(inputString);
     List<String> outputList = postfixConverter.convertToPostfix(inputList);
     fileHandler.fromStringListToFile(outputList, "C:\\Users\\dasza\\Projects\\pg\\AiSD\\AiSDLab5\\src\\main\\java\\lab5\\infixToPostfix.txt");

        System.out.println("Infix: " + inputList);

        System.out.println("Postfix: " + outputList);

        System.out.println();
        System.out.println();


        PostfixCalculator postfixCalculator = new PostfixCalculator();
        String inputStringForCalc = fileHandler.fromFileToString("C:\\Users\\dasza\\Projects\\pg\\AiSD\\AiSDLab5\\src\\main\\java\\lab5\\postfixInput.txt");
        List<String> inputListForCalc = inputAnalyser.convertFromStringToListForPostfix(inputStringForCalc);
        System.out.println("Input: " + inputListForCalc);
        System.out.println("Result: " + postfixCalculator.calculatePostfix(inputListForCalc));

   /*     PostfixCalculator postfixCalculator = new PostfixCalculator();
        LinkedList<String> testlist = new LinkedList<>();
        testlist.add("6");
        testlist.add("5");
        testlist.add("15");
        testlist.add("*");
        testlist.add("+");
        System.out.println(postfixCalculator.calculatePostfix(testlist));

        LinkedList<String> secondTestList = new LinkedList<>();
        secondTestList.add("6");
        secondTestList.add("5");
        secondTestList.add("-");
        secondTestList.add("8");
        secondTestList.add("4");
        secondTestList.add("-");
        secondTestList.add("*");
        secondTestList.add("4");
        secondTestList.add("3");
        secondTestList.add("-");
        secondTestList.add("/");
        System.out.println(postfixCalculator.calculatePostfix(secondTestList)); */


    }
}
