package lab5;

public enum Operator {
    LEFT_PARENTHESIS("(", 2),
    RIGHT_PARENTHESIS(")", 2),
    MULTIPLY("*", 1),
    DIVIDE("/", 1),
    ADD("+", 0),
    SUBSTRACT("-", 0);

    private String value;
    private int precedence;

    public int getPrecedence() {
        return precedence;
    }

    Operator(String value, int precedence) {
        this.value = value;
        this.precedence = precedence;
    }
}
