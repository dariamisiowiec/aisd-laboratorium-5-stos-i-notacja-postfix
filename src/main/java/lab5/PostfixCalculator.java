package lab5;

import java.util.List;

public class PostfixCalculator {

    private InputAnalyser inputAnalyser;

    public PostfixCalculator(){
        inputAnalyser = new InputAnalyser();
    }

    public double calculatePostfix(List<String> inputList) {
        Stack<String> stack = new Stack<>();
        for (int i = 0; i < inputList.size(); i++) {
            if (inputAnalyser.isOperand(inputList.get(i))) {
                stack.push(inputList.get(i));
            }
            else {
                String secondTemp = stack.pop();
                String firstTemp = stack.pop();
                Double firstNumber = Double.parseDouble(firstTemp);
                Double secondNumber = Double.parseDouble(secondTemp);
                Double tempResult = 0.;
                switch (inputList.get(i)) {
                    case "+":
                        tempResult = firstNumber + secondNumber;
                        break;
                    case "-":
                        tempResult = firstNumber - secondNumber;
                        break;
                    case "*":
                        tempResult = firstNumber * secondNumber;
                        break;
                    case "/":
                        tempResult = firstNumber / secondNumber;
                        break;
                }
                String tempResultInString = String.valueOf(tempResult);
                stack.push(tempResultInString);
            }
        }

        double result = Double.parseDouble(stack.pop());
        return result;
    }

}
