package lab5;

import java.util.ArrayList;
import java.util.List;

public class Stack <T> {

    private List<T> list;

    public Stack() {
        list = new ArrayList<T>();
    }

    public void push (T object) {
        list.add(object);
    }

    public T pop() {
        if (!isEmpty()) {
            T object = list.get(list.size() - 1);
            list.remove(list.size() - 1);
            return object;
        }
        return null;
    }

    public T peek() {
        if(!isEmpty())
            return list.get(list.size()-1);
        return null;
    }

    public boolean isEmpty() {
        if (list.size() == 0)
            return true;
        return false;
    }

    public int size(){
        return list.size();
    }
}
